package model

import (
	uuid "github.com/satori/go.uuid"
	"gorm.io/gorm"
)

func NewMember(fname, lname, email, role string) *Member {
	return &Member{
		Firstname: fname,
		Lastname:  lname,
		Email:     email,
		Role:      role,
	}
}

type Member struct {
	gorm.Model
	UUID       string `json:"uuid" gorm:"uniqueIndex:member_uuid"`
	MerchantID string `json:"-" gorm:"not null"`
	Firstname  string `json:"firstname"`
	Lastname   string `json:"lastname"`
	Email      string `json:"email" gorm:"column:email"`
	Role       string `json:"role"`
}

// BeforeCreate hooks for the create
func (m *Member) BeforeCreate(*gorm.DB) error {
	u := uuid.Must(uuid.NewV4(), nil)
	m.UUID = u.String()
	return nil
}
