package model

import (
	uuid "github.com/satori/go.uuid"
	"gorm.io/gorm"
)

func NewMerchant(name, mid string) *Merchant {
	return &Merchant{
		Name:       name,
		MerchantID: mid,
	}
}

// Merchant ...
type Merchant struct {
	gorm.Model
	Name       string    `json:"name"`
	MerchantID string    `json:"merchant_id" gorm:"column:merchant_id; uniqueIndex:merchant_id"`
	UUID       string    `json:"uuid" gorm:"column:uuid; uniqueIndex:merchant_uuid"`
	Code       string    `json:"code" gorm:"column:code; uniqueIndex:merchant_code"`
	Logo       string    `json:"logo"`
	Members    []*Member `json:"ForeignKey:MerchantID;"`
}

// BeforeCreate hooks for the create
func (m *Merchant) BeforeCreate(*gorm.DB) error {
	u := uuid.Must(uuid.NewV4(), nil)
	m.UUID = u.String()
	m.Code = u.String()
	return nil
}
