package service

import (
	"bitbucket.com/onboarding/internal/db/repository"
	"bitbucket.com/onboarding/internal/model"
)

// Merchant is interface of merchant service
type Merchant interface {
	Query(int, int) ([]*model.Merchant, int64, error)
	Create(*model.Merchant) error
	Update(string, map[string]interface{}) (*model.Merchant, error)
}

// NewMerchant returns merchant interface
func NewMerchant(
	merchantRepo repository.Merchant,
) Merchant {
	return &merchantImpl{
		repositories: struct {
			merchant repository.Merchant
		}{
			merchant: merchantRepo,
		},
	}
}

type merchantImpl struct {
	repositories struct {
		merchant repository.Merchant
	}
}

func (s *merchantImpl) Query(offset, limit int) ([]*model.Merchant, int64, error) {
	items, err := s.repositories.merchant.Query(offset, limit)
	if err != nil {
		return nil, 0, err
	}
	count, err := s.repositories.merchant.TotalCount()
	if err != nil {
		return nil, 0, err
	}
	return items, count, nil
}

func (s *merchantImpl) Create(ifce *model.Merchant) error {
	err := s.repositories.merchant.Create(ifce)
	if err != nil {
		return err
	}
	return nil
}

func (s *merchantImpl) Update(uuid string, data map[string]interface{}) (*model.Merchant, error) {
	merchant, err := s.repositories.merchant.Update(uuid, data)
	if err != nil {
		return nil, err
	}
	return merchant, nil
}
