package service

import (
	"bitbucket.com/onboarding/internal/db/repository"
)

// Member is interface of member service
type Member interface{}

// NewMember returns member interface
func NewMember(
	memberRepo repository.Member,
) Member {
	return &memberImpl{
		repositories: struct {
			member repository.Member
		}{
			member: memberRepo,
		},
	}
}

type memberImpl struct {
	repositories struct {
		member repository.Member
	}
}
