package errors

var Error struct {
	Code    string
	Message string
}
