package sql

import (
	"database/sql"
	"errors"
	"testing"

	"bitbucket.com/onboarding/internal/model"
	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// TestMerchant is entrypoint for merchant repository
func TestMerchant(t *testing.T) {
	suite.Run(t, new(MerchantSuite))
}

type MerchantSuite struct {
	suite.Suite
	mock       sqlmock.Sqlmock
	DB         *gorm.DB
	repository *Merchant
}

func (s *MerchantSuite) SetupMerchantSuite() {
	var (
		db  *sql.DB
		err error
	)

	db, s.mock, err = sqlmock.New()
	require.NoError(s.T(), err)

	dialector := postgres.New(postgres.Config{Conn: db})
	gormdb, err := gorm.Open(dialector, &gorm.Config{})
	require.NoError(s.T(), err)

	s.repository = &Merchant{Model: &model.Merchant{}, OmitForUpdate: []string{}, Gorm: gormdb}
}

func (s *MerchantSuite) AfterTest(_, _ string) {
	require.NoError(s.T(), s.mock.ExpectationsWereMet())
}

func (s *MerchantSuite) Test_Query() {
	// --- Test query without error
	s.Run("query", func() {
		mockmerchants := sqlmock.NewRows([]string{"id"}).AddRow(1)
		s.mock.
			ExpectQuery("SELECT (.+) FROM `merchants`$").
			WillReturnRows(mockmerchants)

		// run our function
		_, err := s.repository.Query(0, 10)
		require.NoError(s.T(), err)
	})

	// --- Test query with error
	s.Run("query with error", func() {
		s.mock.
			ExpectQuery("SELECT (.+) FROM `merchants`$").
			WillReturnError(errors.New("fake error"))

		// run our function
		_, err := s.repository.Query(0, 10)
		require.Error(s.T(), err)
	})
}
