package sql

import (
	"bitbucket.com/onboarding/internal/model"
	"gorm.io/gorm"
)

func NewMerchant(gorm *gorm.DB) *Merchant {
	return &Merchant{
		Gorm:          gorm,
		Model:         &model.Merchant{},
		OmitForUpdate: []string{},
	}
}

type Merchant struct {
	Gorm          *gorm.DB
	Model         interface{}
	OmitForUpdate []string
}

// Query returns list
func (repo *Merchant) Query(offset, limit int) ([]*model.Merchant, error) {
	var items []*model.Merchant
	if err := repo.Gorm.
		Model(&model.Merchant{}).
		Offset(offset).
		Limit(limit).
		Find(&items).Error; err != nil {
		return nil, err
	}
	return items, nil
}

// TotalCount returns count
func (repo *Merchant) TotalCount() (int64, error) {
	var total int64
	if err := repo.Gorm.
		Model(&model.Merchant{}).
		Count(&total).Error; err != nil {
		return 0, err
	}
	return total, nil
}

// Create item
func (repo *Merchant) Create(ifce *model.Merchant) error {
	return repo.Gorm.
		Model(&model.Merchant{}).Create(&ifce).Error
}

// Update item
func (repo *Merchant) Update(uuid string, data map[string]interface{}) (*model.Merchant, error) {
	merchant := &model.Merchant{}
	if err := repo.Gorm.
		Model(merchant).
		Omit(repo.OmitForUpdate...).
		Where("uuid = ?", uuid).
		Updates(data).Error; err != nil {
		return nil, err
	}
	return merchant, nil
}

// Save item
func (repo *Merchant) Save(uuid string, ifce *model.Merchant) error {
	if err := repo.Gorm.
		Model(&model.Merchant{}).
		Omit(repo.OmitForUpdate...).
		Where("uuid = ?", uuid).
		Save(ifce).Error; err != nil {
		return err
	}
	return nil
}

// Delete delete item
func (repo *Merchant) Delete(uuid string) error {
	if err := repo.Gorm.
		Model(&model.Merchant{}).
		Where("uuid = ?", uuid).Error; err != nil {
		return err
	}
	return nil
}

// FindByID takes id and return item
func (repo *Merchant) FindByID(id uint) (*model.Merchant, error) {
	var item *model.Merchant
	if err := repo.Gorm.
		Where("id = ?", id).
		Model(&model.Merchant{}).
		First(&item).Error; err != nil {
		return nil, err
	}
	return item, nil
}

// FindByUUID takes uuid and return item
func (repo *Merchant) FindByUUID(uuid string) (*model.Merchant, error) {
	var item *model.Merchant
	if err := repo.Gorm.
		Model(&model.Merchant{}).
		Where("uuid = ?", uuid).
		First(&item).Error; err != nil {
		return nil, err
	}
	return item, nil
}
