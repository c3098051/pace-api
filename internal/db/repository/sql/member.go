package sql

import (
	"bitbucket.com/onboarding/internal/model"
	"gorm.io/gorm"
)

func NewMember(gorm *gorm.DB) *Member {
	return &Member{
		Gorm:          gorm,
		Model:         &model.Member{},
		OmitForUpdate: []string{},
	}
}

type Member struct {
	Gorm          *gorm.DB
	Model         interface{}
	OmitForUpdate []string
}

// Query returns list
func (repo *Member) Query(offset, limit int) ([]*model.Member, error) {
	var items []*model.Member
	if err := repo.Gorm.
		Model(&model.Member{}).
		Offset(offset).
		Limit(limit).
		Find(&items).Error; err != nil {
		return nil, err
	}
	return items, nil
}

// TotalCount returns count
func (repo *Member) TotalCount() (int64, error) {
	var total int64
	if err := repo.Gorm.
		Model(&model.Member{}).
		Count(&total).Error; err != nil {
		return 0, err
	}
	return total, nil
}

// Create item
func (repo *Member) Create(ifce *model.Member) error {
	return repo.Gorm.
		Model(&model.Member{}).Create(&ifce).Error
}

// Update item
func (repo *Member) Update(uuid string, data map[string]interface{}) (*model.Member, error) {
	member := &model.Member{}
	if err := repo.Gorm.
		Model(member).
		Omit(repo.OmitForUpdate...).
		Where("uuid = ?", uuid).
		Updates(data).Error; err != nil {
		return nil, err
	}
	return member, nil
}

// Save item
func (repo *Member) Save(uuid string, ifce *model.Member) error {
	if err := repo.Gorm.
		Model(&model.Member{}).
		Omit(repo.OmitForUpdate...).
		Where("uuid = ?", uuid).
		Save(ifce).Error; err != nil {
		return err
	}
	return nil
}

// Delete delete item
func (repo *Member) Delete(uuid string) error {
	if err := repo.Gorm.
		Model(&model.Member{}).
		Where("uuid = ?", uuid).Error; err != nil {
		return err
	}
	return nil
}

// FindByID takes id and return item
func (repo *Member) FindByID(id uint) (*model.Member, error) {
	var item *model.Member
	if err := repo.Gorm.
		Where("id = ?", id).
		Model(&model.Member{}).
		First(&item).Error; err != nil {
		return nil, err
	}
	return item, nil
}

// FindByUUID takes uuid and return item
func (repo *Member) FindByUUID(uuid string) (*model.Member, error) {
	var item *model.Member
	if err := repo.Gorm.
		Model(&model.Member{}).
		Where("uuid = ?", uuid).
		First(&item).Error; err != nil {
		return nil, err
	}
	return item, nil
}
