package repository

import "bitbucket.com/onboarding/internal/model"

type Member interface {
	Query(int, int) ([]*model.Member, error)
	TotalCount() (int64, error)
	Create(*model.Member) error
	Save(string, *model.Member) error
	Update(string, map[string]interface{}) (*model.Member, error)
	Delete(string) error
	FindByID(uint) (*model.Member, error)
	FindByUUID(string) (*model.Member, error)
}
