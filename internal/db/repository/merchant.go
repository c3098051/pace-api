package repository

import "bitbucket.com/onboarding/internal/model"

type Merchant interface {
	Query(int, int) ([]*model.Merchant, error)
	TotalCount() (int64, error)
	Create(*model.Merchant) error
	Save(string, *model.Merchant) error
	Update(string, map[string]interface{}) (*model.Merchant, error)
	Delete(string) error
	FindByID(uint) (*model.Merchant, error)
	FindByUUID(string) (*model.Merchant, error)
}
