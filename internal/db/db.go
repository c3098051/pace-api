package db

import (
	"bitbucket.com/onboarding/configs"

	"gorm.io/gorm"
)

// New ...
func New(c *configs.DBConfig) (*DB, error) {
	gorm, err := newgorm(c)
	if err != nil {
		return nil, err
	}
	return &DB{gorm: gorm}, nil
}

// DB ...
type DB struct {
	gorm    *gorm.DB
	configs *configs.DBConfig
}

func (db *DB) Gorm() *gorm.DB {
	return db.gorm
}

// Begin ...
func (db *DB) Begin() *gorm.DB {
	return db.gorm.Begin()
}

// Close ...
func (db *DB) Close() {

}

// Clone ...
func (db *DB) Clone(nullableTx *gorm.DB) *DB {
	tx := nullableTx
	if nullableTx == nil {
		tx = db.gorm
	}
	return &DB{gorm: tx}
}
