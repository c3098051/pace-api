package db

import (
	"fmt"

	"bitbucket.com/onboarding/configs"
	"bitbucket.com/onboarding/internal/model"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var (
	models = []interface{}{
		&model.Merchant{},
		&model.Member{},
	}
)

// NewGorm returns *gorm.DB
func newgorm(c *configs.DBConfig) (*gorm.DB, error) {
	dbURL := getpostgresURL(c)
	dialector := postgres.Open(dbURL)
	db, err := gorm.Open(dialector, &gorm.Config{})
	if err != nil {
		return nil, err
	}

	if err := runMigration(db, c); err != nil {
		return nil, err
	}
	return db, nil
}

func getpostgresURL(conf *configs.DBConfig) string {
	return fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=disable",
		conf.Host,
		conf.Port,
		conf.Username,
		conf.Database,
		conf.Password,
	)
}

// AutoMigrate ...
func runMigration(gormdb *gorm.DB, c *configs.DBConfig) error {
	if err := gormdb.AutoMigrate(models...); err != nil {
		return err
	}
	return nil
}
