package endpoint

import (
	"strconv"

	"github.com/labstack/echo/v4"
)

const (
	defaultPage  = 1
	defaultLimit = 10
)

// ParsePaging ...
func ParsePaging(ctx echo.Context) (int, int) {
	var (
		page  = defaultPage
		limit = defaultLimit
	)

	if v := ctx.QueryParams().Get("page"); v != "" {
		if npage, err := strconv.Atoi(v); err != nil {
			page = npage
		}
	}

	if v := ctx.QueryParams().Get("limit"); v != "" {
		if nlimit, err := strconv.Atoi(v); err != nil {
			limit = nlimit
		}
	}

	return page, limit
}
