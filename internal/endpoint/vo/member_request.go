package vo

// QueryMemberRequest is request struct for querying member to merchant
type QueryMemberRequest struct{}

func (r *QueryMemberRequest) Validate() error {
	return nil
}

// AddMemberRequest is request struct for adding member to merchant
type AddMemberRequest struct{}

func (r *AddMemberRequest) Validate() error {
	return nil
}

// UpdateMemberRequest is request struct for updating member to merchant
type UpdateMemberRequest struct{}

func (r *UpdateMemberRequest) Validate() error {
	return nil
}

// DeleteMemberRequest is request struct for deleting member to merchant
type DeleteMemberRequest struct{}

func (r *DeleteMemberRequest) Validate() error {
	return nil
}
