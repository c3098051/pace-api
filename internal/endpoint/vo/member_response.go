package vo

import "bitbucket.com/onboarding/internal/model"

// QueryMemberResponse is response struct for querying members
type QueryMemberResponse struct {
	Items []*model.Member
	Meta  map[string]interface{}
}

// AddMemberResponse is response struct for adding member to merchant
type AddMemberResponse struct{}

// UpdateMemberResponse is response struct for updating member
type UpdateMemberResponse struct{}

// DeleteMemberResponse is response struct for deleting member from merchant
type DeleteMemberResponse struct{}
