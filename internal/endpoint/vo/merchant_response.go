package vo

import "bitbucket.com/onboarding/internal/model"

// QueryMerchantResponse is response struct for querying merchant
type QueryMerchantResponse struct {
	Items []*model.Merchant
	Meta  map[string]interface{}
}
