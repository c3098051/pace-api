package vo

// QueryMerchantRequest is request struct for querying merchant
type QueryMerchantRequest struct{}

func (r *QueryMerchantRequest) Validate() error {
	return nil
}

// CreateMerchantRequest is request struct for creating merchant
type CreateMerchantRequest struct {
	Name       string
	MerchantID string
}

func (r *CreateMerchantRequest) Validate() error {
	return nil
}

// UpdateMerchantRequest is request struct for updating merchant
type UpdateMerchantRequest struct {
	Uuid string
	Data map[string]interface{}
}

func (r *UpdateMerchantRequest) Validate() error {
	return nil
}

// UploadLogoRequest is request struct for updating logo for merchant
type UploadLogoRequest struct {
	Uuid string
}

func (r *UploadLogoRequest) Validate() error {
	return nil
}
