package controller

import (
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"os"
	"time"

	"github.com/getsentry/sentry-go"
	"github.com/labstack/echo/v4"
)

func makeFileFromCtx(ctx echo.Context, key string) (string, error) {
	file, err := ctx.FormFile(key)
	if err != nil {
		return "", err
	}

	var f *os.File
	if f, err = createTempFile(envStoragePath, file); err != nil {
		return "", err
	}
	defer f.Close()

	path, filename, err := storeToDataDir(envStoragePath, f)
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("/%v/%v", path, filename), nil
}

func createTempFile(basedir string, file *multipart.FileHeader) (f *os.File, err error) {
	var src multipart.File
	if src, err = file.Open(); err != nil {
		return nil, err
	}
	defer src.Close()

	// Destination
	var dst *os.File
	dst, err = ioutil.TempFile("", fmt.Sprintf("*_%v", file.Filename))
	if err != nil {
		sentry.CaptureException(err)
		return nil, err
	}

	// Copy
	if _, err = io.Copy(dst, src); err != nil {
		sentry.CaptureException(err)
		return nil, err
	}

	return dst, nil
}

func storeToDataDir(basedir string, f *os.File) (path string, filename string, err error) {
	var subdir string
	if _, subdir, err = ensureSubdir(basedir); err != nil {
		sentry.CaptureException(err)
		return "", "", err
	}

	var st os.FileInfo
	if st, err = f.Stat(); err != nil {
		return "", "", err
	}

	newpath := fmt.Sprintf("%v/%v/%v", basedir, subdir, st.Name())
	if err := moveFile(f.Name(), newpath); err != nil {
		return "", "", err
	}

	return subdir, st.Name(), nil
}

func moveFile(sourcePath, destPath string) error {
	inputFile, err := os.Open(sourcePath)
	if err != nil {
		return fmt.Errorf("Couldn't open source file: %s", err)
	}
	outputFile, err := os.Create(destPath)
	if err != nil {
		inputFile.Close()
		return fmt.Errorf("Couldn't open dest file: %s", err)
	}
	defer outputFile.Close()
	_, err = io.Copy(outputFile, inputFile)
	inputFile.Close()
	if err != nil {
		return fmt.Errorf("Writing to output file failed: %s", err)
	}
	// The copy was successful, so now delete the original file
	err = os.Remove(sourcePath)
	if err != nil {
		return fmt.Errorf("Failed removing original file: %s", err)
	}
	return nil
}

func ensureSubdir(basedir string) (string, string, error) {
	y, m, d := time.Now().Date()
	subdir := fmt.Sprintf("%v%v%v", y, int(m), d)

	dir := fmt.Sprintf("%v/%v", basedir, subdir)
	if _, err := os.Stat(dir); err != nil {
		if err := os.MkdirAll(dir, 0777); err != nil {
			return "", "", err
		}
	}
	return dir, subdir, nil
}
