package controller

import (
	"fmt"

	"bitbucket.com/onboarding/internal/endpoint"
	"bitbucket.com/onboarding/internal/endpoint/vo"
	"bitbucket.com/onboarding/internal/model"
	"bitbucket.com/onboarding/internal/service"
	"github.com/labstack/echo/v4"
)

var (
	formFileLogoKey = "logo"

	// storagePath to store image
	envBasePath    = "127.0.0.1:3000"
	envStoragePath = "./"
)

func HandleQueryMerchant(svc service.Merchant) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		// bind & validate request
		var req = &vo.QueryMerchantRequest{}
		if err := ctx.Bind(req); err != nil {
			return endpoint.BadRequest(ctx, nil)
		}

		// validates
		if err := req.Validate(); err != nil {
			return endpoint.BadRequest(ctx, err)
		}

		page, limit := endpoint.ParsePaging(ctx)
		items, count, err := svc.Query(page, limit)
		if err != nil {
			return endpoint.Internal(ctx, err)
		}

		resp := &vo.QueryMerchantResponse{
			Items: items,
			Meta:  endpoint.ToQueryMeta(page, limit, count),
		}

		// --- success!
		return endpoint.StatusOK(ctx, resp)
	}
}

func HandleCreateMerchant(svc service.Merchant) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		// bind & validate request
		var req = &vo.CreateMerchantRequest{}
		if err := ctx.Bind(req); err != nil {
			return endpoint.BadRequest(ctx, nil)
		}

		// validates
		if err := req.Validate(); err != nil {
			return endpoint.BadRequest(ctx, err)
		}
		merchant := model.NewMerchant(req.Name, req.MerchantID)

		// calling the service
		err := svc.Create(merchant)
		if err != nil {
			return endpoint.Internal(ctx, err)
		}

		// --- success!
		return endpoint.StatusOK(ctx, merchant)
	}
}

func HandleUpdateMerchant(svc service.Merchant) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		// bind & validate request
		var req = &vo.UpdateMerchantRequest{}
		if err := ctx.Bind(req); err != nil {
			return endpoint.BadRequest(ctx, nil)
		}

		// validates
		if err := req.Validate(); err != nil {
			return endpoint.BadRequest(ctx, err)
		}

		// calling the service
		merchant, err := svc.Update(req.Uuid, req.Data)
		if err != nil {
			return endpoint.Internal(ctx, err)
		}

		// --- success!
		return endpoint.StatusOK(ctx, merchant)
	}
}

func HandleUploadLogo(svc service.Merchant) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		// bind & validate request
		var req = &vo.UploadLogoRequest{}
		if err := ctx.Bind(req); err != nil {
			return endpoint.BadRequest(ctx, nil)
		}

		// validates
		if err := req.Validate(); err != nil {
			return endpoint.BadRequest(ctx, err)
		}

		// get file from context, and save to storage path
		filepath, err := makeFileFromCtx(ctx, formFileLogoKey)
		if err != nil {
			return endpoint.BadRequest(ctx, err)
		}
		fullpath := fmt.Sprintf("%v/%v", envBasePath, filepath)

		// calling the service
		data := map[string]interface{}{"logo": fullpath}
		merchant, err := svc.Update(req.Uuid, data)
		if err != nil {
			return endpoint.Internal(ctx, err)
		}

		// --- success!
		return endpoint.StatusOK(ctx, merchant)
	}
}
