package controller

import (
	"bitbucket.com/onboarding/internal/endpoint"
	"bitbucket.com/onboarding/internal/endpoint/vo"
	"bitbucket.com/onboarding/internal/service"
	"github.com/labstack/echo/v4"
)

func HandleQueryMember(svc service.Member) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		// bind & validate request
		var req = &vo.QueryMemberRequest{}
		if err := ctx.Bind(req); err != nil {
			return endpoint.BadRequest(ctx, nil)
		}

		// validates
		if err := req.Validate(); err != nil {
			return endpoint.BadRequest(ctx, err)
		}

		var resp *vo.QueryMemberResponse
		return endpoint.StatusOK(ctx, resp)
	}
}

func HandleAddMember(svc service.Member) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		// bind & validate request
		var req = &vo.AddMemberRequest{}
		if err := ctx.Bind(req); err != nil {
			return endpoint.BadRequest(ctx, nil)
		}

		// validates
		if err := req.Validate(); err != nil {
			return endpoint.BadRequest(ctx, err)
		}

		resp := &vo.AddMemberResponse{}
		return endpoint.StatusOK(ctx, resp)
	}
}

func HandleUpdateMember(svc service.Member) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		// bind & validate request
		var req = &vo.UpdateMemberRequest{}
		if err := ctx.Bind(req); err != nil {
			return endpoint.BadRequest(ctx, nil)
		}

		// validates
		if err := req.Validate(); err != nil {
			return endpoint.BadRequest(ctx, err)
		}

		resp := &vo.UpdateMemberResponse{}
		return endpoint.StatusOK(ctx, resp)
	}
}

func HandleDeleteMember(svc service.Member) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		// bind & validate request
		var req = &vo.DeleteMemberRequest{}
		if err := ctx.Bind(req); err != nil {
			return endpoint.BadRequest(ctx, nil)
		}

		// validates
		if err := req.Validate(); err != nil {
			return endpoint.BadRequest(ctx, err)
		}

		resp := &vo.DeleteMemberResponse{}
		return endpoint.StatusOK(ctx, resp)
	}
}
