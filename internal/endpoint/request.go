package endpoint

import (
	"github.com/labstack/echo/v4"
)

// BindDestination take data from ctx.Context and bind to destination
type BindDestination interface {
	Validate() error
}

func BindAndValidate(ctx echo.Context, dst BindDestination) error {
	if err := ctx.Bind(dst); err != nil {
		return BadRequest(ctx, nil)
	}

	// validate
	if err := dst.Validate(); err != nil {
		return BadRequest(ctx, err)
	}
	return nil
}
