package endpoint

import (
	"math"
	"net/http"

	"github.com/labstack/echo/v4"
)

// Response represents error response
type Response struct {
	Code          int    `json:"code"`
	Message       string `json:"message,omitempty"`
	MessageDetail string `json:"message_detail"`
}

func (r *Response) Clone() *Response {
	return &Response{
		Message: r.Message,
	}
}

// HTTP error statuses with message
var (
	BadRequestResponse   = &Response{Message: http.StatusText(http.StatusBadRequest)}
	UnauthorizedResponse = &Response{Message: http.StatusText(http.StatusUnauthorized)}
	NotFoundResponse     = &Response{Message: http.StatusText(http.StatusNotFound)}
	InternalResponse     = &Response{Message: http.StatusText(http.StatusInternalServerError)}
)

func NotFound(ctx echo.Context, err error) error {
	resp := NotFoundResponse.Clone()
	if err != nil {
		resp.MessageDetail = err.Error()
	}
	return ctx.JSON(http.StatusNotFound, resp)
}

func BadRequest(ctx echo.Context, err error) error {
	resp := NotFoundResponse.Clone()
	if err != nil {
		resp.MessageDetail = err.Error()
	}
	return ctx.JSON(http.StatusBadRequest, resp)
}

func Unauthorized(ctx echo.Context, err error) error {
	resp := NotFoundResponse.Clone()
	if err != nil {
		resp.MessageDetail = err.Error()
	}
	return ctx.JSON(http.StatusUnauthorized, resp)
}

func Internal(ctx echo.Context, err error) error {
	resp := InternalResponse.Clone()
	if err != nil {
		resp.MessageDetail = err.Error()
	}
	return ctx.JSON(http.StatusInternalServerError, resp)
}

func StatusOK(ctx echo.Context, data interface{}) error {
	return ctx.JSON(http.StatusOK, data)
}

// ToQueryMeta return
func ToQueryMeta(page int, limit int, total int64) map[string]interface{} {
	lastPage := int(math.Ceil(float64(total) / float64(limit)))
	meta := map[string]interface{}{
		"current_page": page,
		"first_page":   1,
		"last_page":    lastPage,
		"limit":        limit,
		"total_count":  total,
	}
	return meta
}
