module bitbucket.com/onboarding

go 1.15

require (
	github.com/asaskevich/govalidator v0.0.0-20200907205600-7a23bdc65eef
	github.com/getsentry/sentry-go v0.10.0
	github.com/joho/godotenv v1.3.0
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/echo/v4 v4.2.0
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.8.0
	github.com/spf13/viper v1.7.1
	github.com/swaggo/echo-swagger v1.1.0
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
	gorm.io/driver/postgres v1.0.8
	gorm.io/gorm v1.20.12
)
