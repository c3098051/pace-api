package main

import (
	"os"
	"os/signal"
	"runtime"
	"syscall"

	"bitbucket.com/onboarding/configs"
	"bitbucket.com/onboarding/internal/db"

	"gopkg.in/alecthomas/kingpin.v2"
)

var (
	// cli option
	verbose = kingpin.Flag("verbose", "verbose").Short('v').Default("false").Bool()
)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// Parse the CLI flags and load the config
	kingpin.CommandLine.HelpFlag.Short('h')
	kingpin.Parse()

	// make config from env var
	conf, err := configs.Load()
	if err != nil {
		logger.Fatal(err)
	}

	db, err := db.New(conf.DB)
	if err != nil {
		logger.Fatal(err)
	}

	// create service
	// service := service.New(db)

	// server
	router := NewRouter(db, conf)
	server := NewServer(router, conf.HTTP)
	go server.Start()
	logger.Info("running on port: %v", conf.HTTP)

	// wait for os.signal
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, os.Kill, syscall.SIGTERM)

	// block for os.signal
	sig := <-quit
	logger.Info("quitting.. singal: %v", sig)
}
