package main

import (
	"os"

	"github.com/sirupsen/logrus"
)

// Logger is the main logger that is abstracted in this package.
var logger *logrus.Logger

func init() {
	logger = logrus.New()
	logger.Formatter = &logrus.TextFormatter{}

	// output to stdout
	logger.SetOutput(os.Stdout)

	// set level to debug by default
	logger.SetLevel(logrus.DebugLevel)
}

// SetLevel set the log level
func SetLevel(l logrus.Level) {
	logger.SetLevel(l)
}

// Trace logs a trace message
func Trace(args ...interface{}) {
	logger.Trace(args...)
}

// Tracef logs a formatted trace messsage
func Tracef(format string, args ...interface{}) {
	logger.Tracef(format, args...)
}

// Debug logs a debug message
func Debug(args ...interface{}) {
	logger.Debug(args...)
}

// Debugf logs a formatted debug messsage
func Debugf(format string, args ...interface{}) {
	logger.Debugf(format, args...)
}

// Info logs an informational message
func Info(args ...interface{}) {
	logger.Info(args...)
}

// Infof logs a formatted informational message
func Infof(format string, args ...interface{}) {
	logger.Infof(format, args...)
}

// Error logs an error message
func Error(args ...interface{}) {
	logger.Error(args...)
}

// Errorf logs a formatted error message
func Errorf(format string, args ...interface{}) {
	logger.Errorf(format, args...)
}

// Warn logs a warning message
func Warn(args ...interface{}) {
	logger.Warn(args...)
}

// Warnf logs a formatted warning message
func Warnf(format string, args ...interface{}) {
	logger.Warnf(format, args...)
}

// Fatal logs a fatal error message
func Fatal(args ...interface{}) {
	logger.Fatal(args...)
}

// Fatalf logs a formatted fatal error message
func Fatalf(format string, args ...interface{}) {
	logger.Fatalf(format, args...)
}
