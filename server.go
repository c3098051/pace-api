package main

import (
	// "bitbucket.com/onboarding/rest"
	"context"
	"fmt"

	"net/http"
	"time"
)

// Server is an HTTP server that implements the restistrative Gophish
// handlers, including the dashboard and REST API.
type Server struct {
	server *http.Server
}

// NewServer returns a new instance of the Rest with the
// provided config and options applied.
func NewServer(handler http.Handler, port int) *Server {
	server := &http.Server{
		Handler:        handler,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
		Addr:           fmt.Sprintf("0.0.0.0:%v", port),
	}
	return &Server{server: server}
}

// Start launches the rest server, listening on the configured address.
func (as *Server) Start() {
	if err := as.server.ListenAndServe(); err != nil {
		if err == http.ErrServerClosed {
			return
		}
		return
	}
}

// Shutdown attempts to gracefully shutdown the server.
func (as *Server) Shutdown() {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	if err := as.server.Shutdown(ctx); err != nil {
		return
	}
}
