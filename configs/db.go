package configs

import (
	"errors"

	"github.com/asaskevich/govalidator"
)

var (
	// ErrInvalidDriver ...
	ErrDBInvalidDriver = errors.New("invalid driver")

	// ErrInvalidHost ...
	ErrDBInvalidHost = errors.New("invalid host")

	// ErrInvalidPort ...
	ErrDBInvalidPort = errors.New("invalid port")

	// ErrInvalidUsername ...
	ErrDBInvalidUsername = errors.New("invalid username")

	// ErrDBInvalidName ...
	ErrInvalidName = errors.New("invalid name")
)

// Config stores database access info
type DBConfig struct {
	Driver   string
	Host     string
	Port     string
	Database string
	Username string
	Password string
}

// Validate validates config
func (c *DBConfig) Validate() error {
	if !govalidator.IsPort(c.Port) {
		return ErrDBInvalidPort
	}

	if !govalidator.IsHost(c.Host) {
		return ErrDBInvalidHost
	}
	return nil
}
