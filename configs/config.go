package configs

import (
	"strings"

	"github.com/joho/godotenv"
	"github.com/spf13/viper"
)

var (
	envHTTP          = "HTTP"
	envDBDriver      = "DB_DRIVER"
	envDBPort        = "DB_PORT"
	envDBHost        = "DB_HOST"
	envDBDatabase    = "DB_DATABASE"
	envDBUsername    = "DB_USERNAME"
	envDBPassword    = "DB_PASSWORD"
	envAccessSecret  = "JWT_ACCESS_SECRET"
	envRefreshSecret = "JWT_REFRESH_SECRET"

	defaultValues = map[string]interface{}{
		envHTTP: 3000,

		//db
		envDBDriver:      "postgres",
		envDBPort:        5432,
		envDBHost:        "127.0.0.1",
		envDBDatabase:    "onboarding",
		envDBUsername:    "onboarding",
		envDBPassword:    "onboarding",
		envAccessSecret:  "DEFAULT_SECRET",
		envRefreshSecret: "DEFAULT_SECRET",
	}
)

// Config ...
type Config struct {
	HTTP          int
	DB            *DBConfig
	AccessSecret  string
	RefreshSecret string
}

// Load ...
func Load() (*Config, error) {
	// set default values
	for k, v := range defaultValues {
		viper.SetDefault(strings.ToUpper(k), v)
	}
	godotenv.Load()
	viper.AutomaticEnv()

	c := &Config{
		HTTP: viper.GetInt(envHTTP),
		DB: &DBConfig{
			Driver:   viper.GetString(envDBDriver),
			Host:     viper.GetString(envDBHost),
			Port:     viper.GetString(envDBPort),
			Database: viper.GetString(envDBDatabase),
			Username: viper.GetString(envDBUsername),
			Password: viper.GetString(envDBPassword),
		},
		AccessSecret:  viper.GetString(envAccessSecret),
		RefreshSecret: viper.GetString(envRefreshSecret),
	}

	if err := c.validate(); err != nil {
		return nil, err
	}
	return c, nil
}

// Validate ...
func (c *Config) validate() error {
	if c.HTTP == 0 {
		return errHTTPInvalid
	}

	if err := c.DB.Validate(); err != nil {
		return err
	}
	return nil
}
