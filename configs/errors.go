package configs

import "errors"

// Error
var (
	errHTTPInvalid = errors.New("invalid http port")
)
